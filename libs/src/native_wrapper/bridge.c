
#include <jni.h>
#include "native_wrapper.h"

JNIEXPORT jint JNICALL Java_tester_NativeWrapper_sum(JNIEnv *env, jobject obj, jint a, jint b) {
   return (jint) sum(a, b);
}

JNIEXPORT jint JNICALL Java_tester_NativeWrapper_dif(JNIEnv *env, jobject obj, jint a, jint b) {
   return (jint) dif(a, b);
}
