#!/bin/bash

gcc -c ./native_wrapper.c -o ./dist/native_wrapper.o
ar rcs ./dist/native_wrapper.a ./dist/native_wrapper.o

gcc -c ./bridge.c -o ./dist/bridge.o -I$JAVA_HOME/include -I$JAVA_HOME/include/linux
gcc -shared ./dist/bridge.o ./dist/native_wrapper.a -o ../../compiled/libNativeWrapper.so
