#!/bin/bash

./clean.sh

./compile.sh

cd ./dist

java -Djava.library.path=./libs tester.TesterApplication
