#!/bin/bash

mkdir dist
mkdir dist/libs

$(cd ./libs/src/native_wrapper/ ; fish compile.sh)
cp ./libs/compiled/* ./dist/libs/

javac ./tester/NativeWrapper.java -d ./dist
javac ./tester/TesterApplication.java -d ./dist
