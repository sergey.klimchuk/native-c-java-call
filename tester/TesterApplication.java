package tester;

public class TesterApplication {
    public static void main(String[] args) {
        System.loadLibrary("NativeWrapper");
        var wrapper = new NativeWrapper();
        int res1 = wrapper.sum(1, 10);
        int res2 = wrapper.dif(10, 1);

        System.out.println(res1);
        System.out.println(res2);
    }
}