package tester;

public class NativeWrapper {
    public native int sum(int a, int b);
    public native int dif(int a, int b);
}